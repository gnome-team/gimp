#ifndef __GIT_VERSION_H__
#define __GIT_VERSION_H__

#define GIMP_GIT_VERSION          "GIMP_3_0_0_RC3"
#define GIMP_GIT_VERSION_ABBREV   "9130eb8"
#define GIMP_GIT_LAST_COMMIT_YEAR "2025"

#endif /* __GIT_VERSION_H__ */
